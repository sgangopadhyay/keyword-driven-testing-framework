# Python file for YAML functions

from yaml import load
from yaml.loader import SafeLoader

class Suman_YAML:
    def __init__(self, file_name):
        self.file = file_name
    
    def yaml_reader(self):
        with open(self.file) as file:
            data = load(file, Loader=SafeLoader)
        return data 

