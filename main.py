from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from webdriver_manager.firefox import GeckoDriverManager
from yaml_function import Suman_YAML
from excel_functions import Suman_Excel_Functions


yaml_file_path = "C:\\Users\\sgang\\OneDrive\\Desktop\\Testing_Frameworks\\Keyword_Driven_Testing\\config.yaml"

s = Suman_YAML(yaml_file_path)

driver = webdriver.Firefox(service=Service(GeckoDriverManager().install()))

driver.get(s.yaml_reader()['url'])

driver.implicitly_wait(5)

driver.find_element(by=By.NAME, value=s.yaml_reader()['username_locator']).send_keys(s.yaml_reader()['username'])
driver.find_element(by=By.NAME, value=s.yaml_reader()['password_locator']).send_keys(s.yaml_reader()['password'])
driver.find_element(by=By.XPATH, value=s.yaml_reader()['submitButton_locator']).click()
print("SUCCESS : Login Success with Username {a}".format(a = s.yaml_reader()['username']))   

# Logout from Dashboard
logout_button = driver.find_element(by=By.XPATH, value=s.yaml_reader()['logout_dropdown'])
action = ActionChains(driver)
action.click(on_element=logout_button).perform()
driver.find_element(by=By.LINK_TEXT, value=s.yaml_reader()['logout_button']).click()
driver.quit()
